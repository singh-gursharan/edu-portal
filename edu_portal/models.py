from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    is_student = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)


class ClassRoom(models.Model):
    name = models.CharField(max_length=30, unique=True)


class UserClassRoom(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="classrooms")
    classroom = models.ForeignKey(
        "ClassRoom", on_delete=models.CASCADE, related_name="users"
    )

    class Meta:
        unique_together = (
            "user",
            "classroom",
        )


class Quiz(models.Model):
    title = models.CharField(max_length=20, unique=True)
    classroom = models.ForeignKey(
        "ClassRoom", on_delete=models.CASCADE, related_name="quizes"
    )
    teacher = models.ForeignKey(User, on_delete=models.CASCADE, related_name="quizes")


class Question(models.Model):
    ques = models.CharField(max_length=100)
    choice_1 = models.CharField(max_length=100)
    choice_2 = models.CharField(max_length=100)
    choice_3 = models.CharField(max_length=100)
    choice_4 = models.CharField(max_length=100)
    ans = models.CharField(max_length=100)
    quiz = models.ForeignKey("Quiz", on_delete=models.CASCADE, related_name="question")


class StudentCompletedQuiz(models.Model):
    student = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="quizes_completed"
    )
    quiz = models.ForeignKey(
        "Quiz", on_delete=models.CASCADE, related_name="students_completed"
    )

    class Meta:
        unique_together = (
            "student",
            "quiz",
        )


class StudentQuestion(models.Model):
    student = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="questions"
    )
    question = models.ForeignKey(
        "Question", on_delete=models.CASCADE, related_name="students"
    )
    ans = models.CharField(max_length=100)

    unique_together = (
        "student",
        "question",
    )
