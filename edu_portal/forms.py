from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import get_object_or_404, Http404

from edu_portal.models import (
    User,
    ClassRoom,
    UserClassRoom,
    Question,
    Quiz,
    StudentCompletedQuiz,
    StudentQuestion,
)
from django.core.exceptions import FieldError, ValidationError


class UserRegistrationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True, is_teacher=False, is_student=False):
        user = super().save(commit=False)
        user.is_teacher = is_teacher
        user.is_student = is_student
        if commit:
            user.save()
        return user


class CreateClassForm(forms.ModelForm):
    class Meta:
        model = ClassRoom
        fields = ("name",)


class AddStudentForm(forms.ModelForm):
    student_email = forms.CharField(label="add student")

    class Meta:
        model = UserClassRoom
        fields = ("student_email",)

    def clean(self):
        super(AddStudentForm, self).clean()
        data = self.cleaned_data
        student_email = self.cleaned_data["student_email"]
        student_exist = User.objects.filter(email=student_email).exists()
        if student_exist is False:
            self.add_error("student_email", "Student with such email does not exist")
        return data

    def save(self, classroom, commit=True):
        student_email = self.cleaned_data["student_email"]
        student = User.objects.get(email=student_email)
        user_class = UserClassRoom(user=student, classroom=classroom)
        if commit:
            user_class.save()
        return user_class


class CreateQuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ("ques", "choice_1", "choice_2", "choice_3", "choice_4", "ans")

    def clean(self):
        super(CreateQuestionForm, self).clean()
        data = self.cleaned_data
        ans = self.cleaned_data["ans"]
        choice_list = [
            self.cleaned_data["choice_1"],
            self.cleaned_data["choice_2"],
            self.cleaned_data["choice_3"],
            self.cleaned_data["choice_4"],
        ]
        if ans in choice_list:
            return data
        else:
            self.add_error("ans", "Answer value should be in the above choices")
        return data

    def save(self, quiz, commit=True):
        question = super().save(commit=False)
        question.quiz = quiz
        if commit is True:
            question.save()
        return question


class CreateQuizForm(forms.ModelForm):
    class Meta:
        model = Quiz
        fields = ("title",)

    def save(self, classroom, teacher, commit=True):
        title = self.cleaned_data["title"]
        quiz = Quiz(title=title, classroom=classroom, teacher=teacher)
        if commit is True:
            quiz.save()
        return quiz


class SubmitQuizForm(forms.Form):
    def __init__(self, questions, data=None, *args, **kwargs):
        super(SubmitQuizForm, self).__init__(data, *args, **kwargs)
        self.questions = questions
        for question in questions:
            field_name = "question_%d" % question.pk
            choices = [
                (question.choice_1, ""),
                (question.choice_2, ""),
                (question.choice_3, ""),
                (question.choice_4, ""),
            ]
            ## May nee to pass some initial data, etc:
            self.fields[field_name] = forms.ChoiceField(
                label=question.ques,
                required=False,
                choices=choices,
                widget=forms.RadioSelect,
            )
            # self.fields[field_name] = forms.CharField(
            #     label=question.ques, widget=forms.RadioSelect(choices=choices)
            # )

    def save(self, student, quiz):
        studentquiz = StudentCompletedQuiz(student=student, quiz=quiz)
        studentquiz.save()
        student_question_list = []
        for field_name in self.fields:
            pk = int(field_name[9:])
            question = get_object_or_404(Question, pk=pk)
            ans_submit = self.cleaned_data[field_name]
            student_question = StudentQuestion(
                student=student, question=question, ans=ans_submit
            )
            student_question.save()
            student_question_list.append(student_question)
        return student_question_list
