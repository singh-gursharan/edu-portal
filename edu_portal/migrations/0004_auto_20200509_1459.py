# Generated by Django 3.0.6 on 2020-05-09 14:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('edu_portal', '0003_remove_classroom_user'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userclassroom',
            old_name='class_room',
            new_name='classroom',
        ),
    ]
