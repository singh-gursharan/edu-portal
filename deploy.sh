#! /bin/bash
sudo apt update
sudo apt install python3
sudo apt-get install python3-venv
python3 -m venv edu-venv
source venv/bin/activate
sudo apt install python3-pip
pip3 install -r requirements.txt
python manage.py migrate
python manage.py runserver
