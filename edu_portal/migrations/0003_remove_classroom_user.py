# Generated by Django 3.0.6 on 2020-05-09 14:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('edu_portal', '0002_userclassroom'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='classroom',
            name='user',
        ),
    ]
