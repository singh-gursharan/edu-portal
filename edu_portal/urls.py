"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("accounts/", include("django.contrib.auth.urls"), name="login"),
    path("signup/<usertype>", views.signup, name="signup"),
    path("", views.redirect_home, name="home"),
    path("<username>/student/", views.home, name="student_home"),
    path("<username>/teacher/", views.home, name="teacher_home"),
    path(
        "<username>/teacher/classes/<class_id>/",
        views.teacher_class,
        name="teacher_class",
    ),
    path(
        "<username>/teacher/classes/<class_id>/add",
        views.add_student_to_class,
        name="add_student",
    ),
    path(
        "<username>/teacher/classes/<class_id>/add_quiz",
        views.add_quiz,
        name="add_quiz",
    ),
    path(
        "<username>/teacher/classes/<class_id>/quiz/<quiz_id>/add_question",
        views.add_question,
        name="add_question",
    ),
    path(
        "<username>/teacher/classes/<class_id>/quizes/<quiz_id>",
        views.submit_quiz,
        name="submit_quiz",
    ),
    path(
        "<username>/teacher/classes/<class_id>/quizes/<quiz_id>/read_only",
        views.read_student_quiz,
        name="read_student_quiz",
    ),
    path(
        "<username>/teacher/classes/<class_id>/quizes/<quiz_id>/teacher/read_only",
        views.read_only_quiz,
        name="read_only_quiz",
    ),
    # this fuction should be designed in such a way that teacher should able to view only and
    # student should be able to submit the quiz.
]
