from django.apps import AppConfig


class EduPortalConfig(AppConfig):
    name = 'edu_portal'
