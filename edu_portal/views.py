from django.shortcuts import render
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect, Http404
from edu_portal.forms import (
    UserRegistrationForm,
    CreateClassForm,
    AddStudentForm,
    CreateQuizForm,
    CreateQuestionForm,
    SubmitQuizForm,
)
from edu_portal.models import (
    ClassRoom,
    UserClassRoom,
    User,
    Quiz,
    StudentCompletedQuiz,
    StudentQuestion,
)
from django.forms import ValidationError
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

# Create your views here.
@login_required
def redirect_home(request):
    user = request.user
    if user.is_teacher == True:
        redirect_string = "teacher_home"
    else:
        redirect_string = "student_home"
    return redirect(redirect_string, username=user.username)


def auth_teacher(func):
    def inner(*arg, **kwargs):
        if arg[0].user.is_teacher == False:
            raise Http404("not teacher error")
        else:
            return func(*arg, **kwargs)

    return inner


def auth_student(func):
    def inner(*arg, **kwargs):
        if arg[0].user.is_student == False:
            raise Http404("not student error")
        else:
            return func(*arg, **kwargs)

    return inner


# def student_signup(request):
#     if request.method == "POST":
#         form = StudentRegistrationForm(request.POST)
#         if form.is_valid():
#             form.save()
#             username = form.cleaned_data.get("username")
#             raw_password = form.cleaned_data.get("password1")
#             user = authenticate(username=username, password=raw_password)
#             login(request, user)
#             return redirect("student_home")
#     else:
#         form = StudentRegistrationForm()
#     return render(request, "registration/signup.html", {"form": form})


def signup(request, usertype):
    if request.method == "POST":
        form = UserRegistrationForm(request.POST)
        redirect_string = ""
        if form.is_valid():
            if usertype == "teacher":
                form.save(is_teacher=True)
                redirect_string = "teacher_home"
            else:
                form.save(is_student=True)
                redirect_string = "student_home"
            username = form.cleaned_data.get("username")
            raw_password = form.cleaned_data.get("password1")
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect(redirect_string, username=user.username)
    else:
        form = UserRegistrationForm()
    return render(request, "registration/signup.html", {"form": form})


@login_required
def student_home(request, username):
    pass


@login_required
def home(request, username):
    if request.method == "POST" and request.user.is_teacher:
        print(request.user.is_teacher)
        form = CreateClassForm(request.POST)
        if form.is_valid():
            classroom = form.save()
            teacher_class = UserClassRoom(user=request.user, classroom=classroom)
            teacher_class.save()
            return redirect("teacher_home", username=username)
        else:
            raise ValidationError("already exist")
    else:
        form = CreateClassForm()
        user = request.user
        user_classrooms = user.classrooms.all()
        classrooms = [x.classroom for x in user_classrooms]
        classroom_list = []
        quizes_completed = []
        if user.is_student:
            user_completed_quizes = user.quizes_completed.all()
            quizes_completed = [x.quiz for x in user_completed_quizes]
        for classroom in classrooms:
            name = classroom.name
            quizes = [x for x in classroom.quizes.all()]
            id = classroom.id
            classroom_list.append((id, name, quizes))
    return render(
        request,
        "edu_portal/home.html",
        {
            "form": form,
            "classroom_list": classroom_list,
            "quizes_completed": quizes_completed,
        },
    )


@login_required
@auth_teacher
def teacher_class(request, username, class_id):
    classroom = get_object_or_404(ClassRoom, pk=class_id)
    user_ids = UserClassRoom.objects.filter(classroom_id=class_id).values_list(
        "user_id", flat=True
    )
    students = User.objects.filter(id__in=user_ids, is_student=True)
    quizes = classroom.quizes.all()
    student_quizes_list = []
    for student in students:
        quiz_list = []
        true_sum = 0
        total_sum = 0
        completion = None
        for quiz in quizes:
            check = StudentCompletedQuiz.objects.filter(
                student=student, quiz=quiz
            ).exists()
            quiz_list.append((check, quiz.id))
            if check:
                true_sum += 1
            total_sum += 1
        if len(quizes) != 0:
            completion = true_sum / total_sum * 100
            completion = round(completion, 2)
        student_quizes_list.append((student, completion, quiz_list))

    return render(
        request,
        "edu_portal/class_result.html",
        {
            "student_quizes_list": student_quizes_list,
            "quizes": quizes,
            "class_id": class_id,
            "class_name": classroom.name,
        },
    )


@method_decorator(csrf_exempt, name="dispatch")
@login_required
@auth_teacher
def add_student_to_class(request, username, class_id):
    class_room = get_object_or_404(ClassRoom, pk=class_id)
    user_classrooms = class_room.users.all()
    students_query_set_list = []
    user_id_list = []
    for x in user_classrooms:
        if x.user.is_student:
            user_id_list.append(x.user.id)
            students_query_set_list.append(x.user)
    students = [x for x in students_query_set_list]
    remaining_student = (
        User.objects.filter(is_student=True).exclude(id__in=user_id_list).all()
    )
    if request.method == "POST":
        form = AddStudentForm(request.POST)
        if form.is_valid():
            form.save(class_room)
            return redirect("add_student", username=username, class_id=class_id)
    else:
        form = AddStudentForm()
    return render(
        request,
        "edu_portal/add_student.html",
        {
            "students": students,
            "remaining_students": remaining_student,
            "form": form,
            "class_room": class_room,
        },
    )


@csrf_exempt
@login_required
@auth_teacher
def add_quiz(request, username, class_id):
    classroom = get_object_or_404(ClassRoom, pk=class_id)
    quiz = None
    if request.method == "POST":
        form = CreateQuizForm(request.POST)
        if form.is_valid():
            quiz = form.save(classroom=classroom, teacher=request.user)
            return redirect(
                "add_question", username=username, class_id=class_id, quiz_id=quiz.id
            )
    else:
        form = CreateQuizForm()
    return render(request, "edu_portal/add_quiz.html", {"form": form})


@login_required
@auth_teacher
def add_question(request, username, class_id, quiz_id):
    quiz = get_object_or_404(Quiz, pk=quiz_id)
    questions = quiz.question.all()
    questions = [x for x in questions]
    question = None
    if request.method == "POST":
        form = CreateQuestionForm(request.POST)
        if form.is_valid():
            question = form.save(quiz=quiz)
            return redirect(
                "add_question", username=username, class_id=class_id, quiz_id=quiz_id
            )
        else:
            return render(
                request,
                "edu_portal/add_ques.html",
                {
                    "form": form,
                    "username": username,
                    "class_id": class_id,
                    "questions": questions,
                    "quiz": quiz,
                },
            )
    else:
        form = CreateQuestionForm()
        if question:
            questions.append(question)
    return render(
        request,
        "edu_portal/add_ques.html",
        {
            "form": form,
            "questions": questions,
            "quiz": quiz,
            "username": username,
            "class_id": class_id,
        },
    )


@csrf_exempt
@login_required
@auth_student
def submit_quiz(request, username, class_id, quiz_id):
    quiz = get_object_or_404(Quiz, pk=quiz_id)
    questions = quiz.question.all()
    if request.method == "POST":
        form = SubmitQuizForm(questions=questions, data=request.POST)
        if form.is_valid():
            form.save(student=request.user, quiz=quiz)
            return redirect("home")
    else:
        form = SubmitQuizForm(questions=questions)
    return render(request, "edu_portal/submit_quiz.html", {"form": form, "quiz": quiz})


@login_required
def read_student_quiz(request, username, class_id, quiz_id):
    user = get_object_or_404(User, username=username)
    quiz = get_object_or_404(Quiz, pk=quiz_id)
    questions = quiz.question.order_by("id").all()
    userquestion = (
        StudentQuestion.objects.filter(question__in=questions, student=user)
        .order_by("question_id")
        .all()
    )
    read_student_quiz_list = []
    for i in range(0, len(userquestion)):
        read_student_quiz_list.append((questions[i], userquestion[i].ans))
    return render(
        request,
        "edu_portal/read_student_quiz.html",
        {"read_student_quiz_list": read_student_quiz_list, "quiz": quiz},
    )


@login_required
def read_only_quiz(request, username, class_id, quiz_id):
    quiz = get_object_or_404(Quiz, id=quiz_id)
    questions = quiz.question.order_by("id").all()
    return render(
        request,
        "edu_portal/read_only_quiz.html",
        {"read_only_quiz": questions, "quiz": quiz},
    )
